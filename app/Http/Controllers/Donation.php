<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SevenPay\PayUnit\PayUnit; 

class Donation extends Controller
{
     public function donate(Request $request)
    {
       
        $amount = $request->input('amount');
        
         $myPayment = New PayUnit("8fed05e7e34c97afc6332a121cc12d8beb87c635","c1968535-1322-40f1-abf2-6ebba10e307d","payunit_sand_Ch8pif04d","http://127.0.0.1:8000/status","http://192.168.100.10/seven-payunit-sandbox/sandbox/notify","test");
        $myPayment->makePayment($amount) ;

        return '';
    }
}
